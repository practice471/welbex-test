resource "aws_lb" "aplication-lb" {
  name               = "welbex-aplication-lb"
  internal           = false
  ip_address_type    = "ipv4"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_securitygroup.id]
  subnets            = data.aws_subnet_ids.default.ids

  tags = {
    Name = "welbex-alb"
  }
}

resource "aws_lb_target_group" "target-group" {
    name = "lb-target-group"
    port = 80
    protocol = "HTTP"
    target_type = "instance"
    vpc_id = data.aws_vpc.default.id

    health_check {
        path                = "/"
        protocol            = "HTTP"
        interval            = 10
        timeout             = 5
        healthy_threshold   = 5
        unhealthy_threshold = 2
    }
}

resource "aws_lb_listener" "http-listener" {
  load_balancer_arn = aws_lb.aplication-lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target-group.arn
  }
}

  resource "aws_lb_target_group_attachment" "name" {
    count            = length(aws_instance.web)
    target_group_arn = aws_lb_target_group.target-group.arn
    target_id        = aws_instance.web[count.index].id
  }