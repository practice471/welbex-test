#!/bin/bash
sudo su
yum update -y
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y
sudo systemctl start docker
sudo docker run -p 80:80 -d public.ecr.aws/y0b7v0i6/welbex-ecr-repo-pub:latest