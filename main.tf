terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region      = "us-east-2"
}

/*resource "aws_ecr_repository" "ecr_repo" {
  name = "welbex-ecr-repo" 
}*/

