resource "aws_instance" "web" {
    ami = "ami-092b43193629811af"
    instance_type = "t2.micro"
    count = 2
    key_name = "welbex-keypair"
    security_groups = ["${aws_security_group.alb_securitygroup.name}"]
    user_data = "${file("init.sh")}"
    
    tags = {
      "Name" = "instance-${count.index}"
    }
}