FROM nginx:alpine

RUN rm /etc/nginx/conf.d/*

COPY helloworld.conf /etc/nginx/conf.d/

COPY index.html /usr/share/nginx/html/